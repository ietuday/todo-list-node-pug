var express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    methodOverride = require('method-override');

// ===========
mongoose.connect('mongodb://localhost:27017/todo-app', { useNewUrlParser: true })
app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(express.static('public'))
app.set('view engine', 'pug');
app.use(methodOverride('_method'));

// Schema / model
var todoSchema = new mongoose.Schema({
    todo: String
})

var Todo = mongoose.model('Todo', todoSchema)

// Routes
app.get('/', (req, res) => {
    Todo.find({}, function (err, foundTodo) {
        if (err) {
            console.log(err);
        } else {
            res.render('index', { todo: foundTodo });
        }
    })
})

app.post('/', (req, res) => {
    Todo.create({ todo: req.body.todo }, function (err, createdTodo) {
        if (err) {
            console.log(err)
        } else {
            res.redirect('/')
        }
    })
})

app.delete('/:id', (req, res) => {
    Todo.findOneAndDelete({ "_id": req.params.id }, function (err) {
        if (err) {
            console.log(err)
        } else {
            res.redirect('/')
        }
    })
})

app.listen(3000, () => {
    console.log('server started at PORT: 3000');
})